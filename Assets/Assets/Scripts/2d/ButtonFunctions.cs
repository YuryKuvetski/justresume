﻿using UnityEngine;
using System.Collections;

public class ButtonFunctions : MonoBehaviour {
	public void PlaySoundEffect (AudioSource soundEffect)
	{
		soundEffect.Stop ();
		soundEffect.Play ();
	}

	public void Exit ()
	{
		Application.Quit ();
	}
	public void OpenNewScene (string SceneName)
	{
		Application.LoadLevel (SceneName);
	}
	public void OpenNewScene (int SceneIndex)
	{
		Application.LoadLevel (SceneIndex);
	}
	public void EnableSomeObject (GameObject obj)
	{
		obj.SetActive(true);
	}
	public void DisableSomeObject (GameObject obj)
	{
		obj.SetActive(false);	
	}
}
