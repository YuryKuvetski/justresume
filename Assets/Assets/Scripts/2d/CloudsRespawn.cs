﻿using UnityEngine;
using System.Collections;

public class CloudsRespawn : MonoBehaviour 
{
	public Transform parentTransform;
	public float minAngle = 2.57f;
	public float maxAngle = 3.925f;
	public Sprite[] cloudSprites;
	public float maxFlyValue = 5.0f;
	public float minFlyValue = -1.0f;
	public float minSpeed = 0.1f;
	public float maxSpeed = 4.0f;
	public float minScale = 0.5f;
	public float maxScale = 5.0f;
	
	float timeForNextChangeDirrection = 0.0f;
	bool isDirrectionDown = false;
	float angle;
	float speed;

	void Start()
	{
		if (cloudSprites.Length != 0) {
			SpriteRenderer sr = GetComponent<SpriteRenderer> ();
			sr.sprite = cloudSprites [Random.Range (0, cloudSprites.Length)];
		}
		speed = Random.Range (minSpeed, maxSpeed); 

		float randomScale = Random.Range (minScale, maxScale);
		parentTransform.localScale = new Vector3 (randomScale,randomScale,1);

		float offsetZ = 0.0f;
		if (maxScale != minScale){
			float scaleRange = Mathf.Abs (maxScale - minScale);
			offsetZ = ((randomScale - minScale)/scaleRange) * 5f;
		}

		parentTransform.position = new Vector3 (parentTransform.position.x, Random.Range(minFlyValue, maxFlyValue), parentTransform.position.z+offsetZ);
		angle = Random.Range (minAngle, maxAngle);
	}

	void Update()
	{
		if (timeForNextChangeDirrection <= 0f) {
			isDirrectionDown = !isDirrectionDown;
			timeForNextChangeDirrection = Random.Range (2.0f, 3.0f);
		}

		if (parentTransform.position.y > maxFlyValue || 
		    parentTransform.position.y < minFlyValue ||
		    angle < minAngle || angle > maxAngle) 
		{
			timeForNextChangeDirrection = 0f;
			angle += ((parentTransform.position.y > maxFlyValue) || angle < minAngle) ? Time.deltaTime : -Time.deltaTime;
		}

		float posX = parentTransform.position.x + 1.2f*Mathf.Cos (angle) * speed * Time.deltaTime;
		float posY = parentTransform.position.y;
		posY += 0.8f*Mathf.Sin (angle) * speed * Time.deltaTime * (speed > 0f ? 1f : -1f);
		parentTransform.position = new Vector3 (posX, posY, parentTransform.position.z);

		timeForNextChangeDirrection -= Time.deltaTime;

		if (parentTransform.position.x <= -20f) {
			DestroyImmediate (gameObject);
		}
	}
}
