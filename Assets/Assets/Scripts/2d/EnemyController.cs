﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour 
{
	public GameObject enemyMesh;
	public ParticleSystem ps;
	public string[] clipNames;
	public Material[] materials;
	public Color[] particleColors;
	public Slider healthSlider;
	public float visibilityRadius = 28f;
	public float torsoRadius = 2f;
	public float speed = 6f;
	public float runFactor = 4f;
	public int attackDamage = 5;
	public int attackUpDamage = 3;
	public int stunDamage = 8;
	public int health = 100;
	
	Animation anim;
	CharacterController  controller;
	Transform playerTransform;
	WarriorController playerController;
	AudioSource HitAS;
	float stopFactor = 0.1f;
	float moveToShiftY;
	float randomTime;
	float tornadoSleep = 0f;
	float startPosZ = 0f;
	bool isRun = false;
	bool isAttacking = false;
	bool isDamaged = false;
	bool isDead = false;
	bool isRotating = false;
	bool isPlayerOnMe = false;
	bool isNeedWaitEndOfAnim = false;	
	bool isNeedHitPlayer = false;
	bool isWalkLeft = true;
	bool isWalk = true;

	public static bool isPlayerDead = false;

	void Start()
	{
		playerController = GameObject.FindGameObjectWithTag ("Player").GetComponent<WarriorController> ();
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		anim = GetComponent<Animation> ();

		controller = GetComponent<CharacterController> ();
		
		GenerateRandomTime ();

		int randomMaterial = 0;
		if (materials.Length != 0) {
			Renderer renderer = enemyMesh.GetComponent<Renderer> ();
			randomMaterial = Random.Range (0, materials.Length);
			renderer.material = materials [randomMaterial];
		}
		if (particleColors.Length != 0) {
			ps.startColor = particleColors [Mathf.Clamp (randomMaterial, 0, particleColors.Length - 1)];
		}

		startPosZ = transform.position.z;

		controller.detectCollisions = false;

		float newRandomSpeed = Random.Range (speed*0.5f, 2f * speed);
		if (newRandomSpeed >= speed * 1.25f) {
			isRun = true;
			speed *= runFactor;
		}

		isWalkLeft = (Random.value % 2 == 0);
		Rotate ();

		healthSlider.maxValue = health;

		HitAS = GetComponent<AudioSource> ();
	}

	void GenerateRandomTime()
	{
		randomTime = Random.Range (3f, 8f);
	}
	void PlayCrossAnimAndWait(int animIndex)
	{
		anim.CrossFade (clipNames [animIndex]);
		isNeedWaitEndOfAnim = true;
	}

	void Update()
	{
		if (isPlayerDead || isDead) {
			isWalk = false;
			Animator ();
			return;
		}

		// Update Data
		float destToPlayerSigned = transform.position.x - playerTransform.position.x;
		float destToPlayer = Mathf.Abs (destToPlayerSigned);
		isPlayerOnMe = destToPlayer < torsoRadius;
		isWalk = destToPlayer <= visibilityRadius && !isPlayerOnMe && !isRotating && !isAttacking;
	
		
		// Update Animation
		Animator ();

		// Check Rotation Dirrection
		bool isNowWalkLeft = transform.position.x >= playerTransform.position.x;
		if (isWalkLeft != isNowWalkLeft && !isRotating) {
			isRotating = true;
			PlayCrossAnimAndWait (isNowWalkLeft ? 10 : 9);	// 10 = "Turn Left"; 9 = "Turn Right";
			isWalkLeft = isNowWalkLeft;
		}

		// Update Position
		float moveX = 0.0f;
		if (isWalk )
			moveX = Time.smoothDeltaTime * speed * (isNeedWaitEndOfAnim ? stopFactor : 1f) * (isWalkLeft ? -1f : 1f);
		controller.Move (new Vector3 (moveX, -0.2f, 0.0f));
			
		if (transform.position.z != startPosZ) {
			transform.position = new Vector3(transform.position.x, transform.position.y, startPosZ);
		}

		// Damage Tornado
		if (tornadoSleep >= 0.5f && isPlayerOnMe && !isNeedHitPlayer && !isNeedWaitEndOfAnim && !isRotating) {
			isNeedHitPlayer = true;
			StartCoroutine ("HitPlayer", 3);	// 3 = "Attack2"
		} else if (isPlayerOnMe) {
			tornadoSleep += Time.smoothDeltaTime;
		}
	}

	void Rotate ()
	{
		if (isWalkLeft)
			transform.localRotation = Quaternion.Euler(0f,  270f, 0f);
		else
			transform.localRotation = Quaternion.Euler(0f,  90f, 0f);
	}

	void Animator()
	{
		if (anim.isPlaying == false) {
			isNeedWaitEndOfAnim = false;
			if (isAttacking) isAttacking = false;
			if (isDamaged) isDamaged = false;
			if (isDead) {
				transform.position -= new Vector3(0, 0.04f,0.0f);
				if (controller.enabled != false){
					controller.enabled = false;
					Destroy(gameObject, 4.0f);
				}
				return;
			}
			if (isRotating) {
				isRotating = false;
				Rotate ();
			}
		}
		if (isDead)	return;

		if (isWalk) {
			if (isNeedWaitEndOfAnim == false)
				anim.CrossFade (isRun ? clipNames [5] : clipNames [4]);	// 5 = "Run"; 4 = "Walk"
		} else {
			if (isNeedWaitEndOfAnim == false)
				anim.CrossFade (clipNames [1]);	//	1 = "Idle"
			
			// Sometimes Make Action
			if (randomTime <= 0f){
				PlayCrossAnimAndWait(0);	// 0 = "Action"
				GenerateRandomTime();
			}else{
				if (isNeedWaitEndOfAnim == false)
					randomTime -= Time.deltaTime;
			}
		}
	}

	IEnumerator HitPlayer(int attackType)
	{
		isAttacking = true;
		PlayCrossAnimAndWait (attackType);
		yield return new WaitForSeconds (0.4f);
		if (isNeedHitPlayer) {
			if (attackType == 2) {	// 2 = "Attack1"
				if (Random.value < 0.98)
					playerController.Hit (attackDamage);
			}
			if (attackType == 3) {	// 3 = "Attack2"
				if (isPlayerOnMe)
					playerController.Hit (attackUpDamage);
				tornadoSleep = 0.0f;
			}
			if (attackType == 8) {	// 8 = "Attack Stun"
				playerController.Hit (stunDamage);
			}
		}

		yield return new WaitForSeconds (0.9f);
		isNeedHitPlayer = false;
	}
	void OnTriggerEnter(Collider other) 
	{
		if (other.CompareTag ("Player") && isAttacking) {
			isNeedHitPlayer = true;
		}
	}
	void OnTriggerStay(Collider other) 
	{
		if (isDead || isPlayerDead || isAttacking) return;

		// Damage player 
		if (other.CompareTag("Player") && !isNeedWaitEndOfAnim && !isNeedHitPlayer) {
			isNeedHitPlayer = true;
			StartCoroutine("HitPlayer", ((Random.value < 0.85f) ? 2 : 8));	// 2 = "Attack1"; 8 = "Attack Stun"
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player")) {
			isNeedHitPlayer = false;
		}
	}

	public bool Hit(int damage)
	{
		if (isDead)
			return false;
		if (isDamaged == false) {
			isDamaged = true;
			health -= damage;
			HitAS.Play();
			ps.Stop();
			ps.Play();
			healthSlider.value = health;
			if (!isAttacking)
				PlayCrossAnimAndWait (7);	// 7 = "Take Damage1"
			if (health <= 0)
				Die ();
			return true;
		}
		return false;
	}
	public void Die()
	{
		if (isDead == false) {
			isDead = true;
			anim.CrossFade (clipNames [6]);	// 6 = "Death"
			isNeedWaitEndOfAnim = true;
		}
	}
}
