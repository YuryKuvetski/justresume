﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	public static bool isGamePaused = false;

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Time.timeScale = Time.timeScale == 0f ? 1f : 0f;
		}
	}
}
