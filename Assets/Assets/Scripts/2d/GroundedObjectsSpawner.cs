﻿using UnityEngine;
using System.Collections;

public class GroundedObjectsSpawner : MonoBehaviour 
{
	public Sprite[] sprites;

	public void Start()
	{
		if (sprites.Length != 0) {
			GetComponent<SpriteRenderer>().sprite = sprites[Random.Range(0, sprites.Length)];
		}
	}

}
