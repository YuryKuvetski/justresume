﻿using UnityEngine;
using System.Collections;

public class HealthSliderRotation : MonoBehaviour 
{
	Quaternion startRotation;

	void Awake ()
	{
		startRotation = transform.rotation;
	}

	void Update ()
	{
		transform.rotation = startRotation;
	}

}
