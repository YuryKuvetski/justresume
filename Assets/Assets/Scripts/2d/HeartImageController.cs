﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HeartImageController : MonoBehaviour 
{
	public Slider hpSlider;

	Animator animator;

	void Start()
	{
		animator = GetComponent<Animator> ();
	}

	short animType = 0;

	public void UpdateAnimator()
	{
		if (hpSlider.normalizedValue >= 0.7f && animType != 0) {
			animType = 0;
			animator.SetTrigger("Stop");
		}else if (hpSlider.normalizedValue < 0.7f && hpSlider.normalizedValue >= 0.5f && animType != 1) {
			animType = 1;
			animator.SetTrigger("Slow");
		}else if (hpSlider.normalizedValue < 0.5f && hpSlider.normalizedValue >= 0.3f && animType != 2) {
			animType = 2;
			animator.SetTrigger("Average");
		}else if (hpSlider.normalizedValue < 0.3f && hpSlider.normalizedValue >= 0f && animType != 3) {
			animType = 3;
			animator.SetTrigger("Fast");
		}
	}
}
