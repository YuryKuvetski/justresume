﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MedallionController : MonoBehaviour 
{
	public ParticleSystem hpUpPS;
	public Slider healthSlider;
	public Slider medallionSlider;
	public float timeForReload = 4f;
	public float healthAdded = 15f;


	public void Start ()
	{
		medallionSlider.maxValue = timeForReload;
		medallionSlider.value = 0f;
	}

	public void Update()
	{
		bool isReloaded = medallionSlider.value >= timeForReload;
		if (!isReloaded) {
			medallionSlider.value += Time.deltaTime;
		} else if (Input.GetKey (KeyCode.E)) {
			medallionSlider.value  = 0f;
			healthSlider.value += healthAdded;
			hpUpPS.Stop();
			hpUpPS.Play();
		}
	}
}
