﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour 
{
	public Slider healthSlider;
	public GameObject playerHitEffect;
	public Camera mainCamera;
	public SwordDamager swordDamager;
	public float speed = 6;
	public float jumpSpeed = 4f;
	public float gravity = 4.0f;
	public float timeForJump = 0.5f;
	public int health = 100;
	public int swordDamage = 18;

	Animation anim;
	CharacterController  controller;
	float cameraOffsetY;
	float stopFactor = 0.5f;
	float jumpTime = 0.0f;
	float mainCameraStartOffsetX;
	float mainCameraFollowSmoothing = 5f;
	float startPosZ = 0f;
	bool isJumping = false;
	bool isAttacking = false;
	bool isDamaged = false;
	bool isDead = false;
	bool isNeedWaitEndOfAnim = false;
	bool isWalkRight = true;
	bool isWalk = true;


	void Start()
	{
		anim = GetComponent<Animation> ();
		controller = GetComponent<CharacterController> ();
		cameraOffsetY = mainCamera.transform.position.y - transform.position.y;
		mainCameraStartOffsetX = mainCamera.transform.position.x;
		
		startPosZ = transform.position.z;

		healthSlider.maxValue = health;
	}

	void Update()
	{
		// Update Data
		float dx = Input.GetAxis ("Horizontal");
		isWalk = Mathf.Abs (dx) >= 0.1f;
		if (dx != 0f)
			isWalkRight = dx > 0f;

		// Update Animation
		Animator ();
		
		// Update Position
		float moveY = 0.5f*CalcJump();
		float moveX = 0.0f;
		if (isWalk) {
			float jumpFactor = moveY > 0 ? (moveY * speed) : 0;
			moveX = Time.smoothDeltaTime * (speed + jumpFactor) * (isNeedWaitEndOfAnim ? stopFactor : 1f) * (isWalkRight ? 1f : -1f);
		}
		controller.Move (new Vector3 (moveX, moveY, 0.0f));
		if (transform.position.z != startPosZ) {
			transform.position = new Vector3(transform.position.x, transform.position.y, startPosZ);
		}

		// Update Rotation
		float localRotateY = transform.localRotation.eulerAngles.y;
		if (isWalkRight && localRotateY > 90f) {
			transform.localRotation = Quaternion.Euler(0f,  localRotateY - 12f, 0f);
		} else if (!isWalkRight && localRotateY < 270f) {
			transform.localRotation = Quaternion.Euler(0f,  localRotateY + 12f, 0f);
		}

		// Update Camera Position
		Vector3 newCamPos = new Vector3 (
			Mathf.Clamp(transform.position.x, mainCameraStartOffsetX, transform.position.x), 
			controller.isGrounded ? (transform.position.y + cameraOffsetY) : mainCamera.transform.position.y, 
			mainCamera.transform.position.z);
		mainCamera.transform.position = Vector3.Lerp (mainCamera.transform.position, newCamPos, mainCameraFollowSmoothing * Time.smoothDeltaTime);
	}

	float CalcJump()
	{
		if (Input.GetButton ("Jump") && controller.isGrounded) {
			jumpTime = 0.0f;
			isJumping = true;
		}
		if (isJumping && jumpTime < timeForJump) {
			jumpTime += Time.smoothDeltaTime;
		} else if (jumpTime > 0.0f) {
			isJumping = false;
			jumpTime -= Time.smoothDeltaTime*(1.0f + jumpTime/timeForJump);
		}
		//return (Time.smoothDeltaTime * ((isJumping ? 1 : -1)*jumpSpeed / timeForJump - (jumpTime <=0 ? gravity : 1.0f))-0.1f);
		return Time.smoothDeltaTime * gravity *((isJumping ? (jumpSpeed) : (-jumpSpeed)) / timeForJump - 1f);
	}

	void Animator()
	{
		// Set Animation States And controll them
		if (anim.isPlaying == false) {
			isNeedWaitEndOfAnim = false;
			if (isAttacking) {
				swordDamager.EndOfAttack();
				isAttacking = false;
			}
			if (isDamaged) isDamaged = false;
			if (isDead) {
				Destroy(gameObject, 0.1f);
				return;
			}
		}

		if (isNeedWaitEndOfAnim == false) {
			if (isWalk) {
				anim.CrossFade ("Walk");
			} else {
				anim.CrossFade ("Wait");
			}
		}
		
		if (Input.GetButton ("Fire1") && isAttacking == false) {
			isAttacking = true;
			anim.CrossFade("Attack");
			isNeedWaitEndOfAnim = true;
		} 
	}

	IEnumerator ShowHitEffect()
	{
		playerHitEffect.SetActive (true);
		yield return new WaitForSeconds (0.1f);
		playerHitEffect.SetActive (false);
	}
	public void Hit(int damage)
	{
		if (isDamaged == false) {
			isDamaged = true;
			health -= damage;
			healthSlider.value = health;
			StartCoroutine("ShowHitEffect");
			anim.Play ("Damage");
			isNeedWaitEndOfAnim = true;
			if (health <= 0)
				Die ();
		}
	}
	public void Die()
	{
		if (isDead == false) {
			isDead = true;
			anim.CrossFade ("Dead");
			isNeedWaitEndOfAnim = true;
			EnemyController.isPlayerDead = true;
		}
	}
	
	public bool IsAtacking()
	{
		return isAttacking;
	}
}