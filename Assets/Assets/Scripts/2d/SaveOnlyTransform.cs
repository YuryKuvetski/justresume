﻿using UnityEngine;
using System.Collections;

public class SaveOnlyTransform : MonoBehaviour 
{
	public Transform transformOfThisMesh;

	void Update ()
	{
		transform.position = transformOfThisMesh.position;
	}

}
