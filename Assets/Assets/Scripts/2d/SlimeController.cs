﻿using UnityEngine;
using System.Collections;

public class SlimeController : MonoBehaviour 
{
	public string[] clipNames;
	public GameObject enemyMesh;
	public Material[] materials;
	public float visibilityRadius = 28f;
	public float speed = 6f;
	public int attackDamage = 1;
	
	Animation anim;
	CharacterController  controller;
	Transform playerTransform;
	WarriorController playerController;
	float stopFactor = 0.1f;
	float moveToShiftY;
	float randomTime;
	float startPosZ = 0f;
	bool isDamaged = false;
	bool isRotating = false;
	bool isNeedWaitEndOfAnim = false;	
	bool isWalkLeft = true;
	bool isWalk = true;
	
	void Start()
	{
		playerController = GameObject.FindGameObjectWithTag ("Player").GetComponent<WarriorController> ();
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		anim = GetComponent<Animation> ();
		
		controller = GetComponent<CharacterController> ();
		controller.detectCollisions = false;

		Renderer renderer = enemyMesh.GetComponent<Renderer> ();
		renderer.material = materials[Random.Range (0, materials.Length)];
		
		startPosZ = transform.position.z;
		
		GenerateRandomTime ();
	
		speed = Random.Range (speed*0.5f, 2f * speed);
		
		isWalkLeft = (Random.value % 2 == 0);
		Rotate ();
	}
	
	void GenerateRandomTime()
	{
		randomTime = Random.Range (3f, 8f);
	}
	void PlayCrossAnimAndWait(int animIndex)
	{
		anim.CrossFade (clipNames [animIndex]);
		isNeedWaitEndOfAnim = true;
	}
	
	void Update()
	{
		if (EnemyController.isPlayerDead) {
			isWalk = false;
			Animator ();
			return;
		}
		
		// Update Data
		float destToPlayer = Mathf.Abs (transform.position.x - playerTransform.position.x);
		isWalk = destToPlayer <= visibilityRadius && !isRotating;
		
		
		// Update Animation
		Animator ();
		
		// Check Rotation Dirrection
		bool isNowWalkLeft = transform.position.x >= playerTransform.position.x;
		if (isWalkLeft != isNowWalkLeft && !isRotating) {
			isRotating = true;
			PlayCrossAnimAndWait (isNowWalkLeft ? 10 : 9);	// 10 = "Turn Left"; 9 = "Turn Right";
			isWalkLeft = isNowWalkLeft;
		}
		
		// Update Position
		float moveX = 0.0f;
		if (isWalk )
			moveX = Time.smoothDeltaTime * speed * (isNeedWaitEndOfAnim ? stopFactor : 1f) * (isWalkLeft ? -1f : 1f);
		controller.Move (new Vector3 (moveX, -0.2f, 0.0f));
		if (transform.position.z != startPosZ) {
			transform.position = new Vector3(transform.position.x, transform.position.y, startPosZ);
		}
	}
	
	void Rotate ()
	{
		if (isWalkLeft)
			transform.localRotation = Quaternion.Euler(0f,  270f, 0f);
		else
			transform.localRotation = Quaternion.Euler(0f,  90f, 0f);
	}
	
	void Animator()
	{
		if (anim.isPlaying == false) {
			isNeedWaitEndOfAnim = false;
			if (isDamaged) isDamaged = false;
			if (isRotating) {
				isRotating = false;
				Rotate ();
			}
		}
		if (isWalk) {
			if (isNeedWaitEndOfAnim == false)
				anim.CrossFade (clipNames [4]);	// 5 = "Run"; 4 = "Walk"
		} else {
			if (isNeedWaitEndOfAnim == false)
				anim.CrossFade (clipNames [1]);	//	1 = "Idle"
		
			// Sometimes Make Action
			if (randomTime <= 0f){
				PlayCrossAnimAndWait(0);	// 0 = "Action"
				GenerateRandomTime();
			}else{
				if (isNeedWaitEndOfAnim == false)
					randomTime -= Time.smoothDeltaTime;
			}
		}
	}

	void OnTriggerStay(Collider other) 
	{
		if (EnemyController.isPlayerDead) return;
		
		// Damage player 
		if (other.CompareTag("Player")) {
			if (Random.value <= 0.015f)
				playerController.Hit (attackDamage);
		}
	}
}
