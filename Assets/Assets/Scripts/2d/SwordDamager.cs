﻿using UnityEngine;
using System.Collections;

public class SwordDamager : MonoBehaviour 
{
	int swordDamage;
	bool isNowAttacking = false;
	bool isCanAddDamage = false;
	float timeLeaved = 0.0f;
	float timeForStart = 0.0f;
	WarriorController wc;

	void Awake ()
	{
		wc = GameObject.FindGameObjectWithTag ("Player").GetComponent<WarriorController>();
		swordDamage =  wc.swordDamage;

	}

	void OnTriggerStay(Collider other) 
	{
		if (isCanAddDamage) {
			if (other.CompareTag ("Frog")) {
				if (wc.CheckItCanBeAttacked (other.transform.position.x)) {
					bool isItAttacked = other.gameObject.GetComponent<EnemyController> ().Hit (swordDamage);

					if (isItAttacked)
						EndOfAttack ();
				}
			}
		}
	}

	void Update()
	{
		if (isNowAttacking) {
			if (timeForStart <= 0f) {
				isCanAddDamage = true;
				if (timeLeaved > 0.0f) {
					timeLeaved -= Time.deltaTime;
				} else if (isNowAttacking) {
					EndOfAttack ();
				}
			} else {
				timeForStart -= Time.deltaTime;
			}
		}
	}

	public void EndOfAttack()
	{		
		isCanAddDamage = isNowAttacking = false;
		timeLeaved = 0.0f;
	}
	
	public void StartAttack(float _secondsForAttack, float _timeForStart)
	{
		isNowAttacking = true;
		timeForStart = _timeForStart;
		timeLeaved = _secondsForAttack;
	}
}
