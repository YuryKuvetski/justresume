﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WarriorController : MonoBehaviour 
{
	public ParticleSystem walkPS;
	public Slider healthSlider;
	public GameObject playerHitEffect;
	public Camera mainCamera;
	public float speed = 4;
	public float runFactor = 4f;
	public float gravity = 4.0f;
	public int health = 100;
	public int swordDamage = 18;

	Animation anim;
	CharacterController  controller;
	SwordDamager swordDamager;
	float cameraOffsetY;
	float mainCameraStartOffsetX;
	float mainCameraFollowSmoothing = 5f;
	float startPosZ = 0f;
	bool isJumping = false;
	bool isJumpRunning = false;
	bool isAttacking = false;
	bool isDamaged = false;
	bool isDead = false;
	bool isNeedWaitEndOfAnim = false;
	bool isWalkRight = true;
	bool isWalk = true;
	bool isRun = true;


	void Start()
	{
		anim = GetComponent<Animation> ();
		swordDamager = GetComponent<SwordDamager> ();
		controller = GetComponent<CharacterController> ();
		cameraOffsetY = mainCamera.transform.position.y - transform.position.y;
		mainCameraStartOffsetX = mainCamera.transform.position.x;

		startPosZ = transform.position.z;
	}

	void Update()
	{
		// Update Data
		float dx = Input.GetAxis ("Horizontal");
		isWalk = Mathf.Abs (dx) >= 0.1f;
		if (isWalk && !isJumping)
			walkPS.Play ();
		else
			walkPS.Stop ();

		if (dx != 0f)
			isWalkRight = dx > 0f;

		// Update Animation
		Animator ();
		
		// Update Position
		isRun = !(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift));
		isRun = isRun && !isAttacking && !isDamaged;
		float moveY = 0.5f*CalcJump();
		float moveX = 0.0f;
		if (isWalk) {
			float newSpeed = speed * ((isJumping ? isJumpRunning : isRun) ? runFactor : 1f) ;
			moveX = Time.smoothDeltaTime * newSpeed * (isWalkRight ? 1f : -1f);
		}
		controller.Move (new Vector3 (moveX, moveY, 0.0f));

		if (transform.position.z != startPosZ) {
			transform.position = new Vector3(transform.position.x, transform.position.y, startPosZ);
		}

		// Update Rotation
		float localRotateY = transform.localRotation.eulerAngles.y;
		if (isWalkRight && localRotateY > 90f) {
			transform.localRotation = Quaternion.Euler(0f,  localRotateY - 12f, 0f);
		} else if (!isWalkRight && localRotateY < 270f) {
			transform.localRotation = Quaternion.Euler(0f,  localRotateY + 12f, 0f);
		}

		// Update Camera Position
		Vector3 newCamPos = new Vector3 (
			Mathf.Clamp(transform.position.x, mainCameraStartOffsetX, transform.position.x), 
			controller.isGrounded ? (transform.position.y + cameraOffsetY) : mainCamera.transform.position.y, 
			mainCamera.transform.position.z);
		mainCamera.transform.position = Vector3.Lerp (mainCamera.transform.position, newCamPos, mainCameraFollowSmoothing * Time.smoothDeltaTime);
	}

	float CalcJump()
	{
		if (Input.GetButton ("Jump") && controller.isGrounded && !isNeedWaitEndOfAnim && !isJumping) {
			isJumping = true;
			isJumpRunning = isRun;
			anim.CrossFade("jump");
			isNeedWaitEndOfAnim = true;
			return 0f;
		}
		if (controller.isGrounded && isNeedWaitEndOfAnim && isJumping) {
			return Time.smoothDeltaTime * gravity;
		}

		if (!isJumping) {
			return -Time.smoothDeltaTime * gravity;
		}

		return 0f;
	}
	
	void Animator()
	{
		// Set Animation States And controll them
		if (anim.isPlaying == false) {
			isNeedWaitEndOfAnim = false;
			if (isAttacking) {
				swordDamager.EndOfAttack();
				isAttacking = false;
			}
			if (isDamaged) isDamaged = false;
			if (isDead) {
				transform.position -= new Vector3(0, 0.02f,0.0f);
				if (controller.enabled != false){
					controller.enabled = false;
					Destroy(gameObject, 4.0f);
				}
				return;
			}
			if (isJumping) {
				isJumping = false;
			}
		}

		if (isNeedWaitEndOfAnim == false) {

			if (isWalk && controller.isGrounded) {
				anim.CrossFade (isRun ? "run" : "walk");
			}else{
				anim.CrossFade ("idle");
			}

			if (Input.GetButton ("Fire1")) {
				isAttacking = true;
				swordDamager.StartAttack(0.2f, 0.3f);
				anim.CrossFade("attack");
				isNeedWaitEndOfAnim = true;
			} 
		}
	}

	IEnumerator ShowHitEffect()
	{
		playerHitEffect.SetActive (true);
		yield return new WaitForSeconds (0.1f);
		playerHitEffect.SetActive (false);
	}
	public void Hit(int damage)
	{
		if (isDamaged == false) {
			isDamaged = true;
			health -= damage;
			healthSlider.value = health;
			StartCoroutine("ShowHitEffect");
			if (!isJumping){
				anim.Play ("hit");
				isNeedWaitEndOfAnim = true;
			}
			if (health <= 0)
				Die ();
		}
	}
	public bool CheckItCanBeAttacked(float posX)
	{
		return ((isWalkRight ? 1 : -1) * (posX-transform.position.x)) >= 0;
	}
	public void Die()
	{
		if (isDead == false) {
			isDead = true;
			anim.CrossFade ("death");
			isNeedWaitEndOfAnim = true;
			EnemyController.isPlayerDead = true;
		}
	}
	
	public bool IsAtacking()
	{
		return isAttacking;
	}
}