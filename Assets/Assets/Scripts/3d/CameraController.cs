﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
    public Transform cameraTarget;
    public Animator playerAC;

    public Transform playerTransform;

    public float distanceToPlayer = 60.0f;
    public float startAngleH = 0.0f;
    public float startAngleV = 1.57f;
    public float rotationSpeedFactor = 1.0f;

    public float minVerticalAngle = 0.2f;
    public float maxVerticalAngle = 1.3f;
    public float minDistanceToPlayer = 2.1f;
    public float maxDistanceToPlayer = 10f;

    float angleV = 0.0f;
    float angleH = 0.0f;

    bool isTurnedNow = false;
    
    int float_AngleBetweenDirrections = Animator.StringToHash("Move_AngleBetweenDirrections");
    int float_TotalSpeed = Animator.StringToHash("TotalSpeed");
    int float_VerticalSpeed = Animator.StringToHash("VerticalSpeed");
    int float_HorizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    int trigger_DoTurn = Animator.StringToHash("Move_DoTurn");

    public void Start()
    {
        angleH = startAngleH;
        angleV = startAngleV;
    }

    public void Update()
    {
        angleH -= Input.GetAxis("Mouse X") * rotationSpeedFactor;
        angleV -= Input.GetAxis("Mouse Y") * rotationSpeedFactor;
        distanceToPlayer -= Input.GetAxis("Mouse ScrollWheel");

        angleV = Mathf.Clamp(angleV, minVerticalAngle, maxVerticalAngle);
        distanceToPlayer = Mathf.Clamp(distanceToPlayer, minDistanceToPlayer, maxDistanceToPlayer);

        transform.position = new Vector3(
            cameraTarget.position.x + distanceToPlayer * Mathf.Cos(angleV) * Mathf.Cos(angleH),
            cameraTarget.position.y + distanceToPlayer * Mathf.Sin(angleV),
            cameraTarget.position.z + distanceToPlayer * Mathf.Cos(angleV) * Mathf.Sin(angleH));

        transform.LookAt(cameraTarget);

        float verticalSpeed = playerAC.GetFloat(float_VerticalSpeed);
        float horizontalSpeed = playerAC.GetFloat(float_HorizontalSpeed);
        float angleBetweenDirections = Mathf.DeltaAngle(playerTransform.rotation.eulerAngles.y, transform.rotation.eulerAngles.y);
        if (verticalSpeed > 0.1f)  // Player rotation follow to camera rotation if and only if the player moves
        {
            float playerRotateToAngle = Mathf.LerpAngle(playerTransform.rotation.eulerAngles.y, transform.rotation.eulerAngles.y, Time.deltaTime);
            playerTransform.rotation = Quaternion.Euler(0f, playerRotateToAngle, 0f);
            // Check for turn
            angleBetweenDirections = Mathf.Abs(angleBetweenDirections);
            if (angleBetweenDirections > 120f && !isTurnedNow)
            {
                playerAC.SetTrigger(trigger_DoTurn);
                isTurnedNow = true;
            }
            else if (angleBetweenDirections < 120f && isTurnedNow)
            {
                isTurnedNow = false;
            }
        }
        else
        {
            angleH += angleBetweenDirections * rotationSpeedFactor / 360f;
        }
    }
}
