﻿using UnityEngine;
using System.Collections;

public class CheckForTurn : StateMachineBehaviour
{
    bool isTurned = false;

    int bool_IsNeedTurn = Animator.StringToHash("Move_IsNeedTurn");
    int float_AngleBetweenDirrections = Animator.StringToHash("Move_AngleBetweenDirrections");

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        float angle = animator.GetFloat(float_AngleBetweenDirrections);
        if ((angle > 130f && !isTurned) || (angle < 50f && isTurned))
        {
            animator.SetBool(bool_IsNeedTurn, true);
            //isTurned = !isTurned;
        }
        else
        {
            animator.SetBool(bool_IsNeedTurn, false);
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
