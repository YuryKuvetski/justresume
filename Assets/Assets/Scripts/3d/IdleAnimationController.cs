﻿using UnityEngine;
using System.Collections;

public class IdleAnimationController : StateMachineBehaviour
{
    public int numberOfClips = 3;
    public float minWaitTime = 2f;
    public float maxWaitTime = 10f;

    bool isCanShowRandomAction = false;
    bool isAnimationSetted = false;
    float timeElapsed = 0.0f;

     // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetInteger("Idle_RandomActionType", 0);
        timeElapsed = Random.Range(minWaitTime, maxWaitTime);
        isAnimationSetted = false;
    }
    
	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (!isAnimationSetted)
        {
            if (timeElapsed <= 0.0f)
            {
                animator.SetInteger("Idle_RandomActionType", Random.Range(1, numberOfClips+1));
                isAnimationSetted = true;
            }
            else
            {
                timeElapsed -= Time.deltaTime;
            }
        }
    }
}
