﻿using UnityEngine;
using System.Collections;

public class Player3rdController : MonoBehaviour 
{
    //
    // NewTypes
    //
    /***********************/
   
    /***********************/

    // 
    // Public Variables
    //
    /***********************/

    /***********************/

    // 
    // Private Variables
    //
    /***********************/
    CharacterController controller;
    Animator AC;

	bool isDead = false;

    float speedV = 0.0f;
    float speedH = 0.0f;
    float totalSpeed = 0.0f;

    // String Hashes
    int float_horizontalSpeed = Animator.StringToHash("HorizontalSpeed");
    int float_verticalSpeed = Animator.StringToHash("VerticalSpeed");
    int float_totalSpeed = Animator.StringToHash("TotalSpeed");
    int trigger_DoJump = Animator.StringToHash("Do_Jump");
    int trigger_SetBlock = Animator.StringToHash("Block_Set");
    int trigger_UnsetBlock = Animator.StringToHash("Block_Unset");
    int bool_IsBlockSetted = Animator.StringToHash("Block_IsSetted");
    /***********************/

    public void Start(){
		controller = GetComponent<CharacterController> ();
        AC = GetComponent<Animator>();
    }

    public void Update()
    {
        CheckInput();

        UpdateMoving();

        UpdateAnimator();
    }

    void CheckInput()
    {
        speedH = Input.GetAxis("Horizontal");
        speedV = Input.GetAxis("Vertical");
        
        if (Input.GetMouseButtonDown(1))    // Right Click for blocking
        {
            AC.SetBool(bool_IsBlockSetted, true);
            AC.ResetTrigger(trigger_UnsetBlock);
            AC.SetTrigger(trigger_SetBlock);
        }
        else if (Input.GetMouseButtonUp(1))
        {
            AC.SetBool(bool_IsBlockSetted, false);
            AC.ResetTrigger(trigger_SetBlock);
            AC.SetTrigger(trigger_UnsetBlock);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<PlayerHealth>().Hit(1);
        }
    }

    void UpdateMoving()
    {
        totalSpeed = new Vector2(speedH, speedV).magnitude;
    }

    void UpdateAnimator()
    {
        AC.SetFloat(float_horizontalSpeed, speedH);
        AC.SetFloat(float_verticalSpeed, speedV);
        AC.SetFloat(float_totalSpeed, totalSpeed);
    }
}
