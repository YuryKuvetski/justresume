﻿using UnityEngine;
using System.Collections;

public class PlayerAttackController : MonoBehaviour
{
    public float timeNeededForComboLoad = 1f;
    public Material[] materialsForComboGlowEffect;
    public float loadGlowFactor = 1.0f;
    public float unloadGlowFactor = 3.0f;
    public float maxGlow = 5.0f;

    Animator AC;
    Color emissionColor = new Color(0.01f, 0.01f, 0.01f);

    int trigger_CanAttack = Animator.StringToHash("Attack_CanAttack");
    int bool_IsCanMakeCombo = Animator.StringToHash("Attack_IsCanMakeCombo");
    int bool_IsBlockSetted = Animator.StringToHash("Block_IsSetted");

    float timeForComboLoading = 0f;

    public void Start()
    {
        AC = GetComponent<Animator>();
    }

    public void Update()
    {
        if (Input.GetButton("Fire1"))   // Hold Fire1 button
        {
            timeForComboLoading += Time.deltaTime;

            float emission = Mathf.Clamp(timeForComboLoading * loadGlowFactor, 0.01f, maxGlow);
            emissionColor = new Color(emission, emission, emission);
            foreach (Material mat in materialsForComboGlowEffect)
            {
                mat.SetColor("_EmissionColor", emissionColor);
            }
        }
        else if (Input.GetButtonDown("Fire1"))  // Press down Fire1 button
        {
            timeForComboLoading = 0f;
        }
        else if (Input.GetButtonUp("Fire1"))    // Unpress Fire1 button
        {
            if (AC.GetBool(bool_IsBlockSetted) == false)
            {
                AC.SetBool(bool_IsCanMakeCombo, (timeForComboLoading >= timeNeededForComboLoad)); // Set true, if combo was loaded
                AC.SetTrigger(trigger_CanAttack);
            }
            timeForComboLoading = 0.0f;
        }
        else if (emissionColor.r > 0.01f)
        {
            float emission = emissionColor.r - Time.deltaTime * unloadGlowFactor;
            emissionColor.r = emissionColor.g = emissionColor.b = (emission <= 0.01f) ? 0.01f : emission;
            foreach (Material mat in materialsForComboGlowEffect)
            {
                mat.SetColor("_EmissionColor", emissionColor);
            }
        }


    }
}
