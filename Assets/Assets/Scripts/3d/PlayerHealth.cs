﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour
{
    public float health;

    Animator AC;

    int bool_IsBlocking = Animator.StringToHash("Block_IsSetted");
    int trigger_DoHit = Animator.StringToHash("Do_Hit");

    public void Start()
    {
        AC = GetComponent<Animator>();
    }

    public void Hit(float damage)
    {
        if (AC.GetBool(bool_IsBlocking) == false)
        {
            health -= damage;
        }
        AC.SetTrigger(trigger_DoHit);
    }
}
