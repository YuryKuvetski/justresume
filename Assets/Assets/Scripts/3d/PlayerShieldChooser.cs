﻿using UnityEngine;
using System.Collections;

public class PlayerShieldChooser : MonoBehaviour
{
    public GameObject[] shieldsPrefabs;
    public int startShieldIndex = 0;

    MeshFilter meshFilter;
    MeshRenderer meshRenderer;
    
    int currentShield = -1;

    public void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        ChangeShield(startShieldIndex);
    }

    public void ChangeShield(int newShieldIndex)
    {
        if (shieldsPrefabs != null)
        {
            if (newShieldIndex != currentShield && newShieldIndex >= 0 && newShieldIndex < shieldsPrefabs.Length)
            {
                meshFilter.mesh = shieldsPrefabs[newShieldIndex].GetComponent<MeshFilter>().sharedMesh;
                meshRenderer.material = shieldsPrefabs[newShieldIndex].GetComponent<MeshRenderer>().sharedMaterial;
                transform.localPosition = shieldsPrefabs[newShieldIndex].transform.localPosition;
                transform.localRotation = shieldsPrefabs[newShieldIndex].transform.localRotation;
                transform.localScale = shieldsPrefabs[newShieldIndex].transform.localScale;
                currentShield = newShieldIndex;
            }
        }
    }
}
