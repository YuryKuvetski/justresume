﻿using UnityEngine;
using System.Collections;

public class TerrainGenerator3D : MonoBehaviour 
{
	public GameObject[] platformsPrefabs;
	public float maxDistanceBetweenPlatforms = 5f;
	public float maxHeightDistanceBetweenPlatforms = 5f;
	public float maxPlatformRotationAngle = 45f;

	float[] platformsBounds;
	Vector3 lastGeneratedPos = Vector3.zero;
	Quaternion lastGeneratedRotation = Quaternion.identity;
	float lastForwardDirrectionAngle = 0;

	public void Start()
	{	
		Random.seed = System.DateTime.Now.Millisecond;
		platformsBounds = new float[platformsPrefabs.Length];
		int i = 0;
		foreach (GameObject obj in platformsPrefabs) {
			float x = obj.GetComponent<BoxCollider>().size.x;
			float z = obj.GetComponent<BoxCollider>().size.z;
			platformsBounds[i] = Mathf.Sqrt(x*x+z*z)*0.5f;
			i++;
		}

		Instantiate (platformsPrefabs[0], lastGeneratedPos, lastGeneratedRotation);
		lastGeneratedPos = GenerateNextPlatformPosition(0,1);
		Instantiate (platformsPrefabs[1], lastGeneratedPos, lastGeneratedRotation);
		lastGeneratedPos = GenerateNextPlatformPosition(1,2);
		Instantiate (platformsPrefabs[2], lastGeneratedPos, lastGeneratedRotation);
		lastGeneratedPos = GenerateNextPlatformPosition(2,3);
		Instantiate (platformsPrefabs[3], lastGeneratedPos, lastGeneratedRotation);
	}

	Vector3 GenerateNextPlatformPosition(int prevPlatfom, int nextPlatform)
	{
		float distance = Random.Range (1f, maxDistanceBetweenPlatforms);
		float distShift = platformsBounds [prevPlatfom] + platformsBounds [nextPlatform] + distance;

		Vector3 newRandomDirrection = new Vector3(
			Mathf.Cos (lastForwardDirrectionAngle * Mathf.Deg2Rad),
			Random.Range(-maxHeightDistanceBetweenPlatforms,maxHeightDistanceBetweenPlatforms),
			Mathf.Sin(lastForwardDirrectionAngle * Mathf.Deg2Rad));
		
		lastGeneratedRotation.eulerAngles = new Vector3 (0f,-lastForwardDirrectionAngle, 0f);

		Vector3 shift =  new Vector3(
			newRandomDirrection.x * distShift,
			newRandomDirrection.y,
			newRandomDirrection.z * distShift);

		float angleShift = Random.Range (-maxPlatformRotationAngle, maxPlatformRotationAngle);
		lastForwardDirrectionAngle += angleShift;

		return lastGeneratedPos + shift;
	}
}
